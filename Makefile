RNCS = devbook.rnc glsa.rnc metadata.rnc mirrors.rnc projects.rnc \
       repositories.rnc userinfo.rnc

ifneq ($(PV),)
P=nxml-gentoo-schemas-$(PV)
else
P=nxml-gentoo-schemas-$(shell TZ=UTC date '+%Y%m%d')
endif

.PHONY: all dist clean
.PRECIOUS: $(RNCS)

all: $(RNCS)

dist: Makefile LICENCE schemas.xml $(RNCS)
	tar -cJf $(P).tar.xz --transform='s%^%$(P)/%' $^

clean:
	rm -f *.tar.xz

%.rnc: dtd/%.dtd
	trang -I dtd -O rnc $< $@

devbook.rnc: devmanual/devbook.rnc
	cp $< $@
