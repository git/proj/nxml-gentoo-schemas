# Used by [gentoo]/xml/htdocs/proj/en/overlays/repositories.xml

# The source copy and history of this file is available from
# https://gitweb.gentoo.org/proj/repositories-xml-format.git/

# Gentoo repositories.xml DTD 1.0
# 
# Modelling by
#   Robert Buchholz <rbu@gentoo.org>
#   Sebastian Pipping <sebastian@pipping.org>
#   Mounir Lamouri <volkmar@gentoo.org>
#   Tiziano Müller <dev-zero@gentoo.org>
# 
# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2 or later

default namespace = ""
namespace a = "http://relaxng.org/ns/compatibility/annotations/1.0"

repositories = element repositories { attlist.repositories, repo* }
attlist.repositories &=
  [ a:defaultValue = "1.0" ] attribute version { string "1.0" }?
repo =
  element repo {
    attlist.repo,
    name,
    description+,
    longdescription*,
    homepage?,
    owner+,
    source+,
    feed*
  }
attlist.repo &=
  attribute priority { text }?,
  attribute quality {
    "core" | "stable" | "testing" | "experimental" | "graveyard"
  },
  attribute status { "official" | "unofficial" }
name = element name { attlist.name, text }
attlist.name &= empty
description = element description { attlist.description, text }
attlist.description &= attribute lang { text }?
longdescription =
  element longdescription { attlist.longdescription, text }
attlist.longdescription &= attribute lang { text }?
homepage = element homepage { attlist.homepage, text }
attlist.homepage &= empty
owner = element owner { attlist.owner, email, name? }
attlist.owner &= attribute type { "project" | "person" }?
source = element source { attlist.source, text }
attlist.source &=
  attribute type {
    "bzr"
    | "cvs"
    | "darcs"
    | "git"
    | "mercurial"
    | "rsync"
    | "svn"
    | "tar"
  }
feed = element feed { attlist.feed, text }
attlist.feed &= empty
email = element email { attlist.email, text }
attlist.email &= empty
start = repositories
